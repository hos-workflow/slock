static const char *colorname[NUMCOLS] = {
	[BACKGROUND] =   "black",     /* after initialization */
	[INIT] =   "#2d2d2d",     /* after initialization */
	[INPUT] =  "#295340",   /* during input */
	[FAILED] = "#cc3333",   /* wrong password */
	[CAPS] = "#ff7700",         /* capslock on */
};
